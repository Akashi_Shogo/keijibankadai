package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.ToCommentList;
import exception.SQLRuntimeException;

public class CommentDao {

	// 新規コメント投稿メソッド
	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("message_id");
			sql.append(", user_id");
			sql.append(", text");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // message_id
			sql.append(", ?"); // user_id
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getMessageId());
			ps.setInt(2, comment.getUserId()); // ？get.UserId
			ps.setString(3, comment.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<ToCommentList> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.user_id as user_id,");
			sql.append("comments.message_id as message_id,");
			sql.append("comments.text as text, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_at as created_at ");
			sql.append("FROM comments ");

			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_at limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<ToCommentList> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	// commentsテーブルの取得
	private List<ToCommentList> toUserCommentList(ResultSet rs) throws SQLException {

		List<ToCommentList> ret = new ArrayList<ToCommentList>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				int message_id = rs.getInt("message_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp created_at = rs.getTimestamp("created_at");

				ToCommentList comment = new ToCommentList();
				comment.setId(id);
				comment.setUserId(user_id);
				comment.setMessageId(message_id);
				comment.setName(name);
				comment.setComment(text);
				comment.setCreated_at(created_at);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void deleteComment(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("DELETE FROM comments WHERE id = ?"); // id以降に削除したいidの指定

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}