package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.ToUserList;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class ToUserListDao {

	public List<ToUserList> getUserList(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users ORDER BY branch_id, department_id");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<ToUserList> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<ToUserList> toUserList(ResultSet rs) throws SQLException {

		List<ToUserList> ret = new ArrayList<ToUserList>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				Timestamp created_at = rs.getTimestamp("created_at");
				Timestamp updated_at = rs.getTimestamp("updated_at");
				int is_deleted = rs.getInt("is_deleted");

				ToUserList ToUserList = new ToUserList();
				ToUserList.setId(id);
				ToUserList.setLoginId(loginId);
				ToUserList.setName(name);
				ToUserList.setBranchId(branchId);
				ToUserList.setDepartmentId(departmentId);
				ToUserList.setCreate_at(created_at);
				ToUserList.setUpdate_at(updated_at);
				ToUserList.setIs_deleted(is_deleted);

				ret.add(ToUserList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void switchUser(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append("  is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIs_deleted());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
