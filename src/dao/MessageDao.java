package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.ToMessageList;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("user_id");
			sql.append(", title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // user_id
			sql.append(", ?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getText());
			ps.setString(4, message.getCategory());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<ToMessageList> getUserMessages(Connection connection, int num, String start, String last,
			String search) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.text as text, ");
			sql.append("users.name as name, ");
			sql.append("messages.category as category, ");
			sql.append("messages.created_at as created_at ");
			sql.append("FROM messages ");

			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append(" WHERE messages.created_at >= ? AND messages.created_at <= ? ");

			if (StringUtils.isBlank(search) == false) {
				sql.append("AND messages.category  LIKE ?");
			}
			sql.append("ORDER BY created_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, last + "00:00:00");

			if (StringUtils.isBlank(search) == false) {

				ps.setString(3, "%" + search + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<ToMessageList> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<ToMessageList> toUserMessageList(ResultSet rs) throws SQLException {

		List<ToMessageList> ret = new ArrayList<ToMessageList>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp created_at = rs.getTimestamp("created_at");

				ToMessageList message = new ToMessageList();
				message.setId(id);
				message.setUserId(userId);
				message.setName(name);
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setCreated_at(created_at);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void deleteMessage(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("DELETE FROM messages WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}