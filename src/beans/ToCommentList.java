package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class ToCommentList implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int messageId;
	private int userId;
	private String name;
	private String comment;
	private Timestamp created_at;



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

//	@Override		//DBから取得できてるか確認用
//	public String toString() {
//		return String.format("id=%s, userId=%s, messageId=%s, name=%s comment=%s, created_at=%s", id, userId, messageId,name, comment,created_at );
//	}
}