package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/userList" })
public class ToUserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<beans.ToUserList> users = new UserService().getUserList();

		List<Branch> branches = new BranchService().getBranch(0, null);
		List<Department> departments = new DepartmentService().getDepartment();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.setAttribute("users", users);
		request.getRequestDispatcher("/userList.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User switchUser = new User();

		switchUser.setId(Integer.parseInt(request.getParameter("id")));
		switchUser.setIs_deleted(Integer.parseInt(request.getParameter("is_deleted")));

		new UserService().switchUser(switchUser);

		response.sendRedirect("/akashi_shogo/userList");
	}
}