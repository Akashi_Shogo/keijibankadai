package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User users = (User) session.getAttribute("loginUser");

		Comment comment = new Comment();
		comment.setUserId(users.getId());
		comment.setText(request.getParameter("text"));
		comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));

		if (isValid(request, messages) == true) {

			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {

			session.setAttribute("errorMessages", messages);
			request.setAttribute("comment", comment);
			response.sendRedirect("./");
		}
	}

	// おまけ
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("text");

		if (StringUtils.isBlank(comment) == true) {
			messages.add("コメントを入力してください");
		}
		if (500 < comment.length() == true) {
			messages.add("コメントは500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}