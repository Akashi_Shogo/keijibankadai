package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branch = new BranchService().getBranch(0, null);
		List<Department> department = new DepartmentService().getDepartment();

		request.setAttribute("branches", branch);
		request.setAttribute("departments", department);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);

		if (isValid(request, messages) == true) {

			new UserService().register(signUpUser);
			response.sendRedirect("/akashi_shogo/userList");

		} else {

			List<Branch> branch = new BranchService().getBranch(0, null);
			List<Department> department = new DepartmentService().getDepartment();

			request.setAttribute("branches", branch);
			request.setAttribute("departments", department);
			request.setAttribute("signUpUser", signUpUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private User getSignUpUser(HttpServletRequest request) throws IOException, ServletException {

		User signUpUser = new User();
		signUpUser.setLoginId(request.getParameter("loginId"));
		signUpUser.setPassword(request.getParameter("password"));
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		signUpUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return signUpUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> message) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

		User overlap = new UserService().userOverlap(loginId);

		if (StringUtils.isBlank(password2) == true) {
			message.add("確認用パスワードを入力してください");

			if (!(password.equals(password2)) == true) {
				message.add("パスワードが一致していません");
			}
		}
		if (StringUtils.isBlank(loginId) == true) {
			message.add("ログインIDを入力してください(6字以上、10字以下)");
		} else if (loginId.length() <= 6 || loginId.length() >= 20 == true) {
			message.add("ログインIDは6文字以上、20文字以下です");
		}
		if (loginId.matches("^[a-zA-Z0-9]+$") == false) {
			message.add("ログインIDには半角英数字のみが使用できます");
		}

		if (StringUtils.isEmpty(password) == true) {
			message.add("パスワードを入力してください(6字以上、20字以下)");
		} else if (password.length() <= 6 || password.length() >= 20 == true) {
			message.add("パスワードは6字以上、20字以下です");
		} else if (password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$ ") == true) {
			message.add("パスワードには半角英数字(記号含む)のみが使用できます");
		}

		if (StringUtils.isBlank(name) == true) {
			message.add("名前を入力してください(10字以下)");
		} else if (name.length() >= 10 == true) {
			message.add("名前は10字以下です");
		}

		if ((branchId == 1) && (departmentId < 3) == false) { // 本社勤務で店長、社員
			message.add("支店と役職が一致していません");
		}
		if ((branchId >= 2) && (departmentId > 2) == false) { // 支店勤務で本社役職
			message.add("支店と役職が一致していません");
		}
		// ここにログイン被りのバリデーション

		if (overlap != null) {
			message.add("ログインIDが重複しています");
		}
		if (message.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
