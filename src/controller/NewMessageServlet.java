package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/message.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();
		User users = (User) session.getAttribute("loginUser");

		Message message = new Message();
		message.setUserId(users.getId());
		message.setTitle(request.getParameter("title"));
		message.setText(request.getParameter("text"));
		message.setCategory(request.getParameter("category"));

		if (isValid(request, messages) == true) {

			new MessageService().register(message);
			response.sendRedirect("./");

		} else {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください(30字以下)");
		} else if (title.length() >= 30 == true) {
			messages.add("タイトルは30字以内です");
		}

		if (StringUtils.isBlank(text) == true) {
			messages.add("メッセージを入力してください(1000字以下)");
		} else if (1000 < text.length()) {
			messages.add("メッセージは1000文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください(10字以下)");
		}
		if (category.length() >= 10 == true) {
			messages.add("カテゴリーは10字以内です");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}