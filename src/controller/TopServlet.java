package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.ToCommentList;
import beans.ToMessageList;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String start = request.getParameter("start");
		String last = request.getParameter("last");

		if (StringUtils.isEmpty(start) == true && StringUtils.isEmpty(last) == true) {
			start = ("2017-11-01-00:00:00");
			last = ("9999-12-31-00:00:00");

		} else if (StringUtils.isEmpty(start) == true) {
			start = ("2017-11-01-00:00:00");

		} else if (StringUtils.isEmpty(last) == true) {
			last = ("9999-12-31-00:00:00");
		}

		String search = request.getParameter("categorySearch");

		List<ToMessageList> messages = new MessageService().getMessage(start, last, search);
		List<ToCommentList> comments = new CommentService().getComment();

		request.setAttribute("start", start);
		request.setAttribute("last", last);
		request.setAttribute("search", search);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/top.jsp").forward(request, response);

	}
}