package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String settingId = request.getParameter("id");
		List<String> messages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest) request).getSession();

		if (StringUtils.isEmpty(settingId)) { // 空打ち
			String message = "不正アクセスです";
			session.setAttribute("errorMessages", message);
			response.sendRedirect("userList");
			return;
		}

		if (settingId.matches("^[0-9]*$") == false) { // 数字以外
			String message = "不正アクセスです";
			session.setAttribute("errorMessages", message);
			response.sendRedirect("userList");
			return;
		}

		User editUser = new UserService().getUser(Integer.parseInt(settingId));

		if (editUser == null) { // user_idがない
			String message = "不正アクセスです";
			session.setAttribute("errorMessages", message);
			response.sendRedirect("userList");
			return;
		}

		List<Branch> branch = new BranchService().getBranch(0, null);
		List<Department> department = new DepartmentService().getDepartment();

		session.setAttribute("errorMessages", messages);
		request.setAttribute("editUser", editUser);
		request.setAttribute("branches", branch);
		request.setAttribute("departments", department);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();
		User editUser = getEditUser(request);

		List<Branch> branch = new BranchService().getBranch(0, null);
		List<Department> department = new DepartmentService().getDepartment();

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています");
				// response.sendRedirect("setting");
			}
			response.sendRedirect("/akashi_shogo/userList");
		} else {
			request.setAttribute("editUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("branches", branch);
			request.setAttribute("departments", department);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return editUser;

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		int id = Integer.parseInt(request.getParameter("id"));
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchId"));
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));

		User overlap = new UserService().userOverlap(loginId);

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください(6字以上、20字以下)");
		}
		if ((loginId.length() <= 6) || (loginId.length() >= 20) == true) {
			messages.add("ログインIDは6字以上、20字以下です");
		}
		if (overlap != null && overlap.getId() != id) {
			messages.add("ログインIDが重複しています");
		}
		if (password2 != null && (!(password.equals(password2)))) {
			messages.add("パスワードが一致していません");
		}

		if (!StringUtils.isBlank(password)) {

			if (password.length() <= 6) {
				messages.add("パスワードは6文字以上、20字以下です");
			}
			if (password.length() >= 20) {
				messages.add("パスワードは6文字以上、20字以下です");
			}
		}
		if (password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$ ")) {
			messages.add("パスワードには半角英数字(記号含む)のみが使用できます");
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください(10字以下)");
		} else if (name.length() >= 10 == true) {
			messages.add("名前は10字以下です");
		}
		if ((branchId == 1) && (departmentId < 3) == false) { // 本社勤務で店長、社員
			messages.add("支店と役職が一致していません");
		}
		if ((branchId >= 2) && (departmentId > 2) == false) { // 支店勤務で本社役職
			messages.add("支店と役職が一致していません");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}