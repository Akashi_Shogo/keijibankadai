<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
<div class="main-contents">
<h1>ユーザー管理</h1>
<div class="linkArea">
	<c:if test="${ not empty loginUser }">
	<a href="./">ホーム</a> /
	<a href="message">メッセージ投稿</a>
	<c:if test="${loginUser.branchId ==1 &&  loginUser.departmentId ==1}">
	  / <a href="userList">ユーザー管理</a>
	  / <a href="signup">ユーザー登録</a>
	</c:if>
	 / <a href="logout">ログアウト</a>
	</c:if>
	</div>
<div class="userList">


	<div class="errorMessages">
			<c:forEach items="${errorMessages}" var="message">
			=====エラー表示=====<br>
				<li><c:out value="${message}" />
			</c:forEach>
	</div>

<table class="userList" border=1>
	<tr>
		<th>ログインID
		<th>名前
		<th>支店
		<th>役職
		<th>復活停止
	</tr>
<c:forEach items="${users}" var="users">
	<tr>
		<td>${users.loginId}</td>
		<td><a href="setting?id=${users.id}" >${users.name}</a></td>
  		<td><c:forEach items="${branches}" var="branches">
	  		<c:if test="${users.branchId == branches.id}">${branches.name}</c:if></c:forEach></td>
		<td><c:forEach items="${departments}" var="departments">
			<c:if test="${users.departmentId == departments.id}">${departments.name}</c:if></c:forEach></td>
		<td>
		<c:if test="${loginUser.id != users.id }">
		<c:if test="${users.is_deleted == 0}">
			<form action="userList" method="post">
			<input type="submit" value="停止" style="background-color:#48d1cc;" onClick="return confirm('本当にアカウントを【停止】しますか？')">
			<input type="hidden" name="id" value="${users.id}">
			<input type="hidden" name="is_deleted" value="1">
			</form>
		</c:if>
		</c:if>
		<c:if test="${users.is_deleted == 1}">
			<form action="userList" method="post">
			<input type="submit" value="復活" style="background-color:#fa8072;" onClick="return confirm('本当にアカウントを【復活】しますか？')">
			<input type="hidden" name="id" value="${users.id}">
			<input type="hidden" name="is_deleted" value="0">
			</form>
		</c:if>

		</td>
	</tr>
</c:forEach>
</table><br>
</div>
<div class="copyright">copyright(c) Akashi Shogo</div>
</div>
</body>
</html>