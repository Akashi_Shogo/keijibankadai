<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>メッセージ投稿</title>
</head>
<body>
	<div class="main-contents">
		<h1>メッセージ投稿</h1>
		<div class="linkArea">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a> /
	<a href="message">メッセージ投稿</a>
				<c:if
					test="${loginUser.branchId ==1 &&  loginUser.departmentId ==1}">
	  / <a href="userList">ユーザー管理</a>
				</c:if>
 / <a href="logout">ログアウト</a>
			</c:if>
		</div>
		<div class="errorMessages">
			<c:if test="${ not empty errorMessages }">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
					<br />
				</c:forEach>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
		<div class="newUser">
			<form action="message" method="post">
				タイトル
				<p1>(30字以下)</p1>
				<br /> <input name="title" value="${message.title}" id="title"
					size="30" /><br />
				<br /> メッセージ
				<p1>(1000字以下)</p1>
				<br />
				<textarea name="text" rows="15" cols="100" wrap="hard">${message.text}</textarea>
				<br />
				<br /> カテゴリー
				<p1>(10字以下)</p1>
				<br /> <input name="category" value="${message.category}"
					id="category" /><br />
				<br /> <input type="submit" value="投稿する"> <a href="./">戻る</a>
				<br>
			</form>
		</div>
		<div class="copyright">copyright(c) Akashi Shogo</div>
	</div>
</body>
</html>