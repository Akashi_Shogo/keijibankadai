<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<body>
<div class="main-contents">
<h1>ユーザー編集</h1>
<div class="linkArea">
	<c:if test="${ not empty loginUser }">
	<a href="./">ホーム</a> /
	<a href="message">メッセージ投稿</a>
	<c:if test="${loginUser.branchId ==1 &&  loginUser.departmentId ==1}">
	  / <a href="userList">ユーザー管理</a>
	</c:if>
 / <a href="logout">ログアウト</a>
	</c:if>
	</div>

	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>

	<div class="setting">
	<form action="setting" method="post"><br />
	<input type="hidden" name="id" value="${editUser.id}">
	<input type="hidden" name="update_at" value="${editUser.update_at}">

		<label for="login_id">ログインID</label><p1>(6字以上、20字以下)</p1><br>
		<input name="login_id" value="${editUser.loginId}" id="loginId" /><br>

		<label for="password">パスワード</label><p1>(6字以上、20字以下)</p1><br>
		<input name="password" id="password" type= "password"/><br>

		<label for="password2">パスワード</label><p1>(確認用)</p1><br />
		<input name="password2" id="password2" type= "password"/><br>

		<label for="name">名前</label><p1>(10字以下)</p1><br />
		<input name="name" value="${editUser.name}" id="name" /><br>

<!-- セレクトボックス支店 -->
		<c:if test="${editUser.id != loginUser.id}">
		支店<select name="branchId">
		<c:forEach items="${branches}" var="branch">

			<c:if test="${editUser.branchId == branch.id}">
			<option value="${branch.id}" selected="${editUser.branchId}">
			<c:out value="${branch.name}"></c:out></option>
			</c:if>

			<c:if test="${editUser.branchId != branch.id}">
			<option value="${branch.id}" >
			<c:out value="${branch.name}"></c:out></option>
			</c:if>
		</c:forEach>
		</select><br>

<!-- セレクトボックス役職 -->
		役職<select name="departmentId">
		<c:forEach items="${departments}" var="department">
			<c:if test="${editUser.departmentId == department.id}">
			<option value="${department.id}" selected="${editUser.departmentId}">
			<c:out value="${department.name}"></c:out></option>
			</c:if>

			<c:if test="${editUser.departmentId != department.id}">
			<option value="${department.id}">
			<c:out value="${department.name}"></c:out></option>
			</c:if>

		</c:forEach>
		</select>
		</c:if>
<br>
		<input type="submit" value="編集" />
		<a href="userList">戻る</a>
	</form>

<div class="copyright">copyright(c) Akashi Shogo</div>
	</div>
</div>
</body>
</html>