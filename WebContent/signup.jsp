<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<h1>ユーザー登録</h1>
		<div class="linkArea">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a> /
	<a href="message">メッセージ投稿</a>
				<c:if
					test="${loginUser.branchId ==1 &&  loginUser.departmentId ==1}">
	  / <a href="userList">ユーザー管理</a>
				</c:if>
 / <a href="logout">ログアウト</a>
			</c:if>
		</div>
		<c:if test="${ not empty errorMessages }">
=====エラー表示=====<br>
			<div class="errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
					<br />
				</c:forEach>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="signup" method="post">
			<br /> ログインID
			<p1>(6字以上、20字以下)</p1>
			<br> <input name="loginId" id="loginId"
				value="${signUpUser.loginId}" /><br> パスワード
			<p1>(6字以上、20字以下)</p1>
			<br> <input name="password" type="password" id="password" /><br>

			パスワード
			<p1>(確認用)</p1>
			<br> <input name="password2" type="password" id="password2" /><br>

			名前
			<p1>(10字以下)</p1>
			<br> <input name="name" id="name" value="${signUpUser.name}" /><br>

			支店<select name="branchId">
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}" label="${branch.name}"></option>
				</c:forEach>
			</select><br> 役職<select name="departmentId">
				<c:forEach items="${departments}" var="department">
					<option value="${department.id}" label="${department.name}"></option>
				</c:forEach>
			</select><br> <input type="submit" value="登録する" /> <a href="userList">戻る</a><br>

		</form>
		<div class="copyright">copyright(c) Akashi Shogo</div>
	</div>
</body>
</html>
