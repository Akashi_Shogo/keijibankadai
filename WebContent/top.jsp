<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板課題</title>
</head>
<body>
	<div class="main-contents">
		<h1>ホーム</h1>

		<%--リンク部分 --%>
		<div class="linkArea">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a> /
	<a href="message">メッセージ投稿</a>
				<c:if
					test="${loginUser.branchId ==1 &&  loginUser.departmentId ==1}">
	  / <a href="userList">ユーザー管理</a>
				</c:if>
 / <span><a href="logout">ログアウト</a></span>
			</c:if>
		</div>

		<%--エラー表示部分 --%>
		<div class="errorMessage">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" />
					</c:forEach>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>

		<%--検索機能 --%>
		<div class="searchArea">

			<form action="./" method="get">
				日付：<input type="date" name="start" value="${start}">～ <input
					type="date" name="last" value="${last}"><br> <br>
				カテゴリー：<input type="text" value="${search}" name="categorySearch">
				<input type="submit" value="検索する"> <a href="./">リセット</a>
			</form>

		</div>
		<%--新規投稿--%>
		<br>
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<p>
					<c:out value="${message.title}" />
				</p>
				<div class="mSet">
					カテゴリー：
					<c:out value="${message.category}" />
					<br> 名前：
					<c:out value="${message.name}" />
					<br> 日付：
					<fmt:formatDate value="${message.created_at}"
						pattern="yyyy/MM/dd HH:mm" />
					<br>
				</div>
				<div class="mText">
					<c:out value="${message.text}" />
				</div>



		<%--投稿の削除 --%>
		<div class="messageDeleted">
			<c:if test="${ loginUser.id == message.userId }">
				<form action="deleteMessage" method="post">
					<input type="submit" value="投稿削除" onClick="return confirm('本当に投稿を削除しますか？')">
					<input type="hidden" name="id" value="${message.id}">
				</form>
			</c:if>
		</div>
	</div>

	<%--コメント --%>
	<div class="comment">
		<form action="comment" method="post">
			<textarea name="text" cols="70" rows="5" class="text" wrap="hard">${comment.text}</textarea>
			<input type="hidden" name="messageId" value="${message.id}"><br>
			<input type="submit" value="コメントする">(500字以下)
		</form>
		<%--コメント表示部分 --%>
		<c:forEach items="${comments}" var="comment">
			<c:if test="${message.id == comment.messageId}">
				<div class="cText">
					<c:out value="${comment.comment}" />
				</div>
				<div class="cName">
					<c:out value="${comment.name}" /><br>
					<fmt:formatDate value="${comment.created_at}" pattern="yyyy/MM/dd HH:mm" /></div>

				<c:if test="${ loginUser.id == comment.userId }">
					<form action="deleteComment" method="post">
						<input type="submit" value="コメント削除"onClick="return confirm('本当にコメントを削除しますか？')">
						<input type="hidden" name="id" value="${comment.id}">
					</form>
				</c:if>
			</c:if>
		</c:forEach>
	</div>

		</c:forEach>
	</div>
	<div class="copyright">copyright(c) Akashi Shogo</div>
</body>
</html>